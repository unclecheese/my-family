<?php

class MyFamilyAPI extends Controller {

	private static $allowed_actions = array (
		'threads',
		'flag',
		'popularthreads',
		'random',
		'account',
		'upload',
		'handleThread',
		'handlePost'
	);


	private static $url_handlers = array (
		'thread/$ThreadID' => 'handleThread',
		'POST ' => 'handlePost'
	);


	private static $sort_columns = array (
		'created',
		'lastedited'
	);



	public function init() {
		parent::init();

		if(!Member::currentUser()) {
			return $this->httpError(403,'You must log in to use the API');
		}
	}


	public function index(SS_HTTPRequest $r) {
		return $this->httpError(404);
	}


	public function upload(SS_HTTPRequest $r) {
		$this->upload = Upload::create();
        $name = 'file';
        $files = $_FILES[$name];
        $tmpFiles = array();

        // Sort the files out into a list of arrays containing each property
        if(!empty($files['tmp_name']) && is_array($files['tmp_name'])) {
            for($i = 0; $i < count($files['tmp_name']); $i++) {                
                if(empty($files['tmp_name'][$i])) continue;
                $tmpFile = array();
                foreach(array('name', 'type', 'tmp_name', 'error', 'size') as $field) {
                    $tmpFile[$field] = $files[$field][$i];
                }
                $tmpFiles[] = $tmpFile;
            }
        } 
        elseif(!empty($files['tmp_name'])) {            
            $tmpFiles[] = $files;
        }

        $ids = array ();

        foreach($tmpFiles as $tmpFile) {
            if($tmpFile['error']) {
                return $this->httpError(400, $tmpFile['error']);
            }
        
            $ext = pathinfo($tmpFile['name'], PATHINFO_EXTENSION);
            $defaultClass = File::get_class_for_file_extension($ext);            
            $fileObject = Injector::inst()->create($defaultClass);

            try {
                $this->upload->loadIntoFile($tmpFile, $fileObject);
                $ids[] = $fileObject->ID;
            } catch (Exception $e) {
                return $this->httpError(400, $e->getMessage());            
            }

            if ($this->upload->isError()) {
                return $this->httpError(400, implode(' ' . PHP_EOL, $this->upload->getErrors()));
            }
        }
        
        return new SS_HTTPResponse(implode(',', $ids), 200);
	}
	

	public function threads(SS_HTTPRequest $r) {

		if($r->getVar('preset')) {
			return $this->getPreset($r->getVar('preset'));
		}

		$limit = $r->getVar('limit') ?: 20;
		if($limit > 20) $limit = 20;

		$start = $r->getVar('page') ?: 0;

		$sort = $r->getVar('sort') ?: 'lastedited';
		$dir = $r->getVar('dir') ?: 'desc';

		if(!in_array($sort, $this->config()->sort_columns)) {
			$sort = 'LastEdited';
		}

		if(!in_array($dir, array('asc', 'desc'))) {
			$dir = 'asc';
		}

		
		$threads = Thread::get()
					->sort("$sort $dir");

		if($s = urldecode($r->getVar('q'))) {

			$elastic = Injector::inst()->get('SilverStripe\Elastica\ElasticaService');
$json = '{  
   "query":{  
      "bool":{  
         "should":[
         	{
         		"match": {
 					"Title": "'.$s.'"
         		}
         	},
         	{
         		"query_string": {
         			"fields": ["Title^3", "PostContent"],
         			"query": "'.$s.'*"
         		}
         	},
         	{
         		"fuzzy": {
         			"Title": {
         				"value": "'.$s.'"
         			}
         		}
         	}
         ]
      }
   }
}';



			$results = $elastic->search(Convert::json2array($json));
			
			$ids = $results->column('ID');					
				

			if(!empty($ids)) {
				$threads = $threads->byIDs($ids)->sort("FIND_IN_SET(ID,'".implode(',',$ids)."')");
			}
			else {
				$threads = ArrayList::create();
			}
		}
		else if($r->getVar('startDate')) {
			
			$threads = $threads->filter(array(
				'Created:GreaterThanOrEqual' => $r->getVar('startDate'),
				'Created:LessThan' => date('Y-m-d', strtotime('+1 month', strtotime($r->getVar('startDate'))))
			));
				
			$threads = $threads->sort('Created ASC');
		}

		$count = $threads->count();
		$threads = $threads->limit($limit, $start*$limit);
						
		$data = array (
			'results' => array (),
			'has_more' => $count > ($limit * ($start+1))
		);

		foreach($threads as $t) {
			$data['results'][] = $this->createThreadJSON($t);
		}

    	return (new SS_HTTPResponse(
    		Convert::array2json($data), 200
    	))->addHeader('Content-Type', 'application/json');

	}


	public function popularthreads(SS_HTTPRequest $r) {
        $threads = Thread::get()
            ->innerJoin('FavoritedThread', 'ThreadID = Thread.ID')                            
            ->sort('COUNT(Thread.ID)','DESC')
            ->limit(10)
            ->alterDataQuery(function($query) {
                $query->groupby('Thread.ID');
            });

		$data = array (
			'results' => array ()
		);

		foreach($threads as $t) {
			$data['results'][] = $this->createThreadJSON($t);
		}

    	return (new SS_HTTPResponse(
    		Convert::array2json($data), 200
    	))->addHeader('Content-Type', 'application/json');


	}





	protected function getPreset($num) {
		$data = array ('results' => array());
		
		switch($num) {
			// legendary
			case 1:
		        $threads = Thread::get()
		            ->innerJoin('FavoritedThread', 'ThreadID = Thread.ID')                            
		            ->sort('COUNT(FavoritedThread.ID)','DESC')		            
		            ->alterDataQuery(function($query) {
		                $query->groupby('Thread.ID');
		            });
		        break;

			// Longest rants			
			case 2:
				$threads = Thread::get()
							->innerJoin('Post','FirstPostID = Post.ID')
							->sort('LENGTH(Content) DESC')
							->limit(20);
			break;
			
			//Most discussed
			case 3:
		        $threads = Thread::get()
		            ->leftJoin('Post', 'ThreadID = Thread.ID')                            
		            ->sort('COUNT(Post.ID)','DESC')
		            ->limit(20)
		            ->alterDataQuery(function($query) {
		                $query->groupby('Thread.ID');
		            });
		    break;

			// Most profane
		    case 4:
		    	require_once(BASE_PATH.'/backend/code/bad_words.php');
		    	$swears = implode('","', $badwords);
$json = '{  
   "query":{  
      "terms":{  
         "PostContent":["'.$swears.'"],
         "minimum_should_match": 2
      }
   }
}';

	
				$elastic = Injector::inst()->get('SilverStripe\Elastica\ElasticaService');
				$results = $elastic->search(Convert::json2array($json));
				$ids = $results->column('ID');					
				$threads = ArrayList::create();
				if(!empty($ids)) {
					$threads = Thread::get()->byIDs($ids)->sort("FIND_IN_SET(ID,'".implode(',',$ids)."')");
				}

			break;

			// Posts by steve that no one answered
			case 5:
				$ids = DB::query("SELECT ThreadID FROM Post GROUP BY ThreadID HAVING COUNT(*) = 1")->column('ThreadID');
				$threads = Thread::get()
								->byIDs($ids)
								->innerJoin('Post','FirstPostID = Post.ID')
								->filter(array(
									'AuthorID' => 6
								));
			break;

			// Posts by Steve called "huh"
			case 6:
				$threads = Thread::get()
							->innerJoin('Post','FirstPostID = Post.ID')
							->filter(array(
								'AuthorID' => 6,
								'Title:StartsWith' => 'huh'
							));							
			break;
		}


		foreach($threads as $t) {
			$data['results'][] = $this->createThreadJSON($t);
		}

    	return (new SS_HTTPResponse(
    		Convert::array2json($data), 200
    	))->addHeader('Content-Type', 'application/json');
	}


	public function handleThread(SS_HTTPRequest $r) {
		$thread = Thread::get()->byID($r->param('ThreadID'));
		
		if(!$thread) {
			return $this->httpError(404);
		}

		$request = new MyFamilyAPI_ThreadRequest($this, $thread);

		return $request->handleRequest($r, DataModel::inst());
	}


	public function random(SS_HTTPRequest $r) {
		$thread = Thread::get()->sort('RAND()')->first();

		return new SS_HTTPResponse($thread->ID, 200);
	}


	public function flag(SS_HTTPRequest $r) {
        $posts = Post::get()
            ->innerJoin('MarkedPost', 'PostID = Post.ID')                            
            ->sort('COUNT(MarkedPost.ID)','DESC')
            ->filter(array(
            	'Status' => $r->param('ID')
            ))		            
            ->alterDataQuery(function($query) {
                $query->groupby('Post.ID');
            });

        $response = array (
        	'has_more' => false,
        	'results' => array ()
        );

        $dummy = new MyFamilyAPI_ThreadRequest($this, Thread::create());

        foreach($posts as $p) {
        	$response['results'][] = $dummy->createPostJSON($p);
        }

		return (new SS_HTTPResponse(
			Convert::array2json($response), 200
		))->addHeader('Content-Type', 'application/json');			
	}


	public function account (SS_HTTPRequest $r) {
		$m = Member::currentUser();
		if($r->isGET()) {
	    	return (new SS_HTTPResponse(
	    		Convert::array2json(array(
	    			'name' => $m->getName(),
	    			'email' => $m->Email,
	    			'notifications' => $m->EmailNotifications,
	    			'period' => $m->EmailPeriod,
	    			'token' => $m->Token
	    		)), 200
	    	))->addHeader('Content-Type', 'application/json');
		}

		else if($r->isPUT()) {
			parse_str($r->getBody(), $vars);
			

			$m->Name = $vars['Name'];
			$m->Email = $vars['Email'];
			if(!empty($vars['Password'])) {
				$m->Password = $vars['Password'];
			}
			$m->EmailNotifications = isset($vars['EmailNotifications']);
			$m->EmailPeriod = $vars['Period'];

			$m->write();

			$message = 'Your changes have been saved';
	    	return (new SS_HTTPResponse(
	    		Convert::array2json(array('response' => $message)), 200
	    	))->addHeader('Content-Type', 'application/json');			
		}
	}


	public function handlePost(SS_HTTPRequest $r) {
		$title = $r->postVar('Title');
		$content = $r->postVar('Content');
		$files = $r->postVar('Files');		

		if($title && $content) {
	    	$thread = Thread::create(array(
	    		'Title' => $title	    		
	    	));
	    	$thread->write();
	    	$post = Post::create(array(
	    		'Content' => $content,
	    		'AuthorID' => Member::currentUserID(),
	    		'ThreadID' => $thread->ID
	    	));
	    	$post->write();	    	
	    	if(!empty($files)) {
	    		$post->Attachments()->setByIDList(explode(',',$files));
	    		$post->write();
	    	}

	    	$thread->FirstPostID = $post->ID;
	    	$thread->write();
			$thread->markAsRead();
	    	
	    	$message = 'Post created';
	    	$code = 200;

		}
		else {
			$message = 'You must provide title and content';
			$code = 400;
		}

    	return (new SS_HTTPResponse(
    		Convert::array2json(array('response' => $message)), $code
    	))->addHeader('Content-Type', 'application/json');
	}


	protected function createThreadJSON(Thread $t) {
		$post = $t->FirstPost();
		return array(
        		'id' => $t->ID,
            	'title' => $t->Title,
            	'summary' => $post->obj('Content')->LimitWordCount(55),
            	'author' => $post->Author()->getName(),
            	'replies' => $t->Posts()->count()-1,
            	'date' => $post->obj('Created')->Format('F j, Y'),
            	'lastEdited' => $t->obj('LastEdited')->Format('F j, Y'),
            	'ago' => $post->obj('Created')->Ago(),
            	'isFavorited' => (boolean) $t->getFavoriteForMember(),
            	'unreadPosts' => $t->getUnreadPostsForMember()->column('ID')
    	);
	}


	public static function parse_youtube($string) {
		return preg_replace(
			"/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
			"<iframe frameborder=\"0\" width=\"100%\" height=\"320\" src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
			$string
		);
	}

}


class MyFamilyAPI_ThreadRequest extends RequestHandler {

	private static $url_handlers = array (
		'post/$PostID!' => 'handlePost',
		'POST favorite' => 'handleFavorite',
		'POST markasread' => 'handleMarkAsRead',
		'GET ' => 'handlePosts',
		'POST ' => 'handleReply'		
	);
	

	private static $allowed_actions = array (
		'handlePost',
		'handlePosts',
		'handleReply',
		'handleFavorite',
		'handleMarkAsRead',
	);



	protected $parent;

	
	protected $thread;


	public function __construct(MyFamilyAPI $parent, Thread $thread) {
		parent::__construct();
		$this->parent = $parent;
		$this->thread = $thread;
	}


	public function handlePosts(SS_HTTPRequest $r) {
		$list = ['results' => []];
		$list['title'] = $this->thread->Title;
		$list['isFavorited'] = (boolean) $this->thread->getFavoriteForMember();
		$list['favorites'] = array ();

		foreach($this->thread->Favorites() as $f) {
			$list['favorites'][] = array (
				'name' => $f->Member()->getName(),
				'date' => $f->obj('Created')->Format('F j, Y')
			);
		}

		foreach($this->thread->Posts() as $p) {
			$list['results'][] = $this->createPostJSON($p);
		}		

    	return (new SS_HTTPResponse(
    		Convert::array2json($list), 200
    	))->addHeader('Content-Type', 'application/json');
	}


	public function handleReply(SS_HTTPRequest $r) {
		$content = $r->postVar('Content');
		$files = $r->postVar('Files');
		if(!empty($content)) {
			$post = Post::create(array('Content' => $content));
			$post->AuthorID = Member::currentUserID();
			$post->ThreadID = $this->thread->ID;
			$post->write();
			$post->Thread()->markAsRead();

	    	if(!empty($files)) {
	    		$post->Attachments()->setByIDList(explode(',',$files));
	    		$post->write();
	    	}
			
	    	return (new SS_HTTPResponse(
	    		Convert::array2json($this->createPostJSON($post)), 200
	    	))->addHeader('Content-Type', 'application/json');
		}
	}


	public function handlePost(SS_HTTPRequest $r) {
		$post = $this->thread->Posts()->byID($r->param('PostID'));
		if(!$post) return $this->httpError(404);

		$request = new MyFamilyAPI_PostRequest($this, $post);

		return $request->handleRequest($r, DataModel::inst());
	}


	public function handleFavorite(SS_HTTPRequest $r) {
		if($existing = $this->thread->getFavoriteForMember()) {
			$existing->delete();
		}
		else {
			$favorite = FavoritedThread::create(array(
				'MemberID' => Member::currentUserID(),
				'ThreadID' => $this->thread->ID
			));
			$favorite->write();
		}

		return new SS_HTTPResponse('OK', 200);
	}


	public function handleMarkAsRead(SS_HTTPRequest $r) {
		$this->thread->markAsRead();

		return new SS_HTTPResponse('OK', 200);
	}


	public function createPostJSON(Post $p) {
		$flags = array ();
		$files = array ();
		foreach($p->Flags() as $f) {
			$flags[] = array (
				'flag' => $f->Status,
				'member' => $f->Member()->getName(),
				'date' => $f->obj('Created')->Format('F j, Y')
			);
		}
		foreach($p->Attachments() as $file) {
			$files[] = array (
				'filename' => $file->Name,
				'url' => $file->getURL(),
				'isImage' => $file->IsImage(),
				'thumbnail' => $file->getPreviewThumbnail(64, 64)->URL
			);
		}
		return array (
			'id' => $p->ID,
			'content' => MyFamilyAPI::parse_youtube($p->obj('Content')->forTemplate()),
			'rawContent' => $p->Content,
			'author' => $p->Author()->getName(),
			'hash' => md5(strtolower($p->Author()->Email)),
			'date' => $p->obj('Created')->Format('F j, Y'),
			'flags' => $flags,
			'files' => $files,
			'threadTitle' => $p->Thread()->Title,
			'threadID' => $p->ThreadID,
			'isUnread' => in_array($p->ID, Member::currentUser()->UnreadPosts()->column('ID')),
			'isOwner' => Member::currentUserID() == $p->AuthorID
		);		
	}
}

class MyFamilyAPI_PostRequest extends RequestHandler {

	private static $url_handlers = array (		
		'PUT flag' => 'handleFlag',
		'GET ' => 'handleGet',
		'PUT ' => 'handlePut',
		'DELETE ' => 'handleDelete'
	);
	

	private static $allowed_actions = array (
		'handleFlag',
		'handleGet',
		'handlePost',
		'handlePut',
		'handleDelete'
	);



	protected $parent;

	
	protected $post;


	public function __construct(MyFamilyAPI $parent, Post $post) {
		parent::__construct();
		$this->parent = $parent;
		$this->post = $post;
	}


	public function handleGet(SS_HTTPRequest $r) {
    	return (new SS_HTTPResponse(
    		Convert::array2json($this->parent->createPostJSON($this->post)), 200
    	))->addHeader('Content-Type', 'application/json');
	}


	public function handlePut(SS_HTTPRequest $r) {
		$post = $this->post;
		parse_str($r->getBody(), $data);

		if(!Member::currentUserID() == $post->AuthorID) return $this->httpError(403);

		if(!empty($data['Content'])) {
			$post->Content = $data['Content'];
			$post->write();
		}
    	
    	return (new SS_HTTPResponse(
    		Convert::array2json($this->parent->createPostJSON($post)), 200
    	))->addHeader('Content-Type', 'application/json');
	}


	public function handleDelete(SS_HTTPRequest $r) {
		$post = $this->post;

		if(!Member::currentUserID() == $post->AuthorID) return $this->httpError(403);
		
		if($post->IsFirstPost()) {
			$threadID = $post->ThreadID;
			Post::get()->byIDs($post->Thread()->Posts()->column('ID'))->removeAll();
			DB::query("DELETE FROM Thread WHERE ID = {$threadID}");
		}
		else {
			$post->delete();
		}

    	return new SS_HTTPResponse('OK', 200);
	}

	public function handleFlag(SS_HTTPRequest $r) {
		$post = $this->post;
		parse_str($r->getBody(), $data);

		if(empty($data['Flag'])) return $this->httpError(404);
		
		$existing = $post->Flags()->filter('MemberID',Member::currentUserID())->first();
		if($existing) $existing->delete();
		$mark = MarkedPost::create(array(
			'Status' => $data['Flag'],
			'MemberID' => Member::currentUserID(),
			'PostID' => $post->ID
		));
		$mark->write();
		$post->flushCache();

    	return (new SS_HTTPResponse(
    		Convert::array2json($this->parent->createPostJSON($post)), 200
    	))->addHeader('Content-Type', 'application/json');
		
    }
}
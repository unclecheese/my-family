/**
 * @jsx React.DOM
 */
// Stylesheet
require('../../../bower_components/bootstrap/dist/css/bootstrap.css');
require('../../assets/css/theme.css');
require('../../assets/css/color-defaults.css');
require('../../assets/css/swatch-black-white.css');
require('../../assets/css/fonts.css');
require('../../css/style.less');


var React = require('react');
window.React = React;

var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;

var App = require('./app');
var Home = require('./pages/Home');
var Detail = require('./pages/Detail');
var FlagPage = require('./pages/FlagPage');
var Main = require('./pages/Main');
var Post = require('./pages/Post');
var Account = require('./pages/Account');
var NotFound = require('./pages/NotFound');
var urls = require('./utils/urlSegmentGenerator');
var request = require('superagent');

var threadRoutes = urls.words.map(function (word) {
	return <Route name={word} path={'/'+word+'/:threadID'} handler={Detail} />
})
var routes = (
	<Route name="app" handler={App}>
		<Route name="main" handler={Main}>
			<Route name="home" handler={Home} path="/" />		
			<Route name="thread" handler={Detail} path="/thread/:threadID" />
			<Route name="flag" handler={FlagPage} path="/flag/:type" />		
			{threadRoutes}
		</Route>
		<Route name="new" path="/new" handler={Post} />
		<Route name="account" path="/account" handler={Account} />
		<Router.NotFoundRoute handler={NotFound} />
	</Route>      
);


Router.run(routes, Router.HistoryLocation, function (Handler) {
  React.render(<Handler/>, document.body);
});


window.setInterval(function() {
	request.get('Security/ping').end(function() {});
},300000);
/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react');
var Dropzone = require('dropzone');

module.exports = React.createClass({displayName: 'Dropzone',

	getInitialState: function () {
		return {
			visible: false
		}
	},

	componentDidMount: function () {
		Dropzone.autoDiscover = false;
		this._dropzone = new Dropzone(this.refs.dropzone.getDOMNode(), this.props);
	},

	componentWillUnmount: function () {
		this._dropzone = null;
	},

	toggleDropzone: function (e) {
		e.preventDefault();
		this.setState({
			visible: !this.state.visible
		});
	},


	render: function () {
		return (
		<div>
			<p>
				<i className="glyphicon glyphicon-paperclip" /> <a href='#' onClick={this.toggleDropzone}>Attach files</a>			
			</p>
			<div className="dropzone" ref="dropzone" style={{display: this.state.visible ? 'block' : 'none'}}></div>			
		</div>
		);
		
		
	}
});

/**
 * @jsx React.DOM
 */
var React = require('react/addons');
var BS = require('react-bootstrap');

module.exports = React.createClass({displayName: 'Star',

	render: function () {
		var empty = !this.props.selected ? '-empty' : '';

	    return (
	    <div className="reply-star">
	    	<a onClick={this.props.onSelect}><i className={"glyphicon glyphicon-star"+empty} /></a>
	    </div>         
	    );
	}
});
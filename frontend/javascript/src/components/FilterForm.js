/**
 * @jsx React.DOM
 */
var React = require('react');
var moment = require('moment');
var BS = require('react-bootstrap');

module.exports = React.createClass({displayName: 'filterform',


    getInitialState: function () {
        return {
            startDate: this.props.startDate
        };
    },


    componentWillReceiveProps: function (nextProps) {        
        this.setState(nextProps);
    },

    handleUpdate: function (e) {                
        return this.goToDate(e.target.value);
    },


    goToDate: function (date) {        
        this.setState({startDate: date}, function () {
            this.props.onUpdate(this.state.startDate);    
        }.bind(this));
        
    },

    canGoBack: function () {
        return this.state.startDate && this.state.startDate != '2003-02-01';
    },

    canGoForward: function () {
        if(!this.state.startDate) return false;

        return moment(this.state.startDate).add(1, 'months').valueOf() < (+new Date());
    },

    prevMonth: function () {
        return this.goToDate(moment(this.state.startDate).subtract(1, 'months').format('YYYY-MM-DD'));
    },

    nextMonth: function () {
      return this.goToDate(moment(this.state.startDate).add(1, 'months').format('YYYY-MM-DD'));  
    },

    createDropdown: function () {
        var m = moment("2003-02-01");
        var d = new Date();
        var years = [];
        var year = 2003;
        while(year <= (new Date().getFullYear())) {
            years.push(year);
            year++;
        }
        return (years.map(function(y, i) {
            var months = [];
            while(m.year() == y || (y == d.getFullYear() && m.month() <= d.getMonth())) {                        
                months.push(
                    <option value={m.format('YYYY-MM-DD')} key={m.format('YYYY-MM-DD')}>
                        {m.format('MMMM')}, {m.year()}
                    </option>
                );
                m.add(1, 'months');
            }
            return (
                <optgroup key={y} label={y}>{months}</optgroup>
            );
        }));

    },

    render: function () {        
        return (
        <form className="filter-form">
            <BS.Row>
                <BS.Col xs={12}>                    
                    <div className="select-wrapper month-filter">
                {this.canGoBack() && 
                    <i onClick={this.prevMonth} className="glyphicon glyphicon-chevron-left left" />
                }

                        <select onChange={this.handleUpdate} className="form-control" value={this.state.startDate || ''} ref="startDate">
                            <option value=""> </option>
                            {this.createDropdown()}
                        </select>
                        <i className="glyphicon glyphicon-chevron-down down" />
                {this.canGoForward() && 
                    <i onClick={this.nextMonth} className="glyphicon glyphicon-chevron-right right" />
                }

                    </div>
                </BS.Col>

            </BS.Row>
        </form>
        );        
    }
});
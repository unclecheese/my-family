/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react/addons');

module.exports = React.createClass({displayName: 'FileList',

	render: function () {
        return (
	        <ul className="file-list">
	            {this.props.files.map(function(f) {
	                return (
	                	<li>
	                		<a target="_blank" href={f.url}>
	                			<span className="file-icon"><img src={f.thumbnail} /></span>
	                			<span className="file-name">{f.filename}</span>
	                		</a>
	                	</li>
	                )
	            })}
	        </ul>
	    );
	}
})

/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react/addons');

module.exports = React.createClass({displayName: 'Post',

	render: function () {
		return (
			<div className="post">
				<strong>{this.props.author}</strong> 
					<div dangerouslySetInnerHTML={{__html: this.props.content}} />
				<div><small>{this.props.date}</small></div>
			</div>
		);
	}
})
var React = require('react');
var PostActions = require('../actions/PostActions');

var EditForm = React.createClass({

	getInitialState: function() {
		return {
			content: this.props.post.content 
		};
	},

	updateContent: function (e) {
		this.setState({
			content: e.target.value
		});
	},

	render: function() {		
		return (
        <form className="edit-form" onSubmit={this.submitHandler}>
            <div className="form-group">
                <textarea ref="content" rows={5} onChange={this.updateContent} className="form-control" value={this.state.content} />
            </div>

            <button type="submit" className="btn btn-primary">Save changes</button>&nbsp;
            <button onClick={this.doCancel} className="btn btn-default">Cancel</button>
        </form>
		);
	},


	submitHandler: function (e) {
		e.preventDefault();
		var p = this.props.post;		
		this.props.onSubmit(p.id, p.threadID, this.state.content);
		PostActions.edit(p.id, p.threadID, this.state.content);
	},


	doCancel: function (e) {
		e.preventDefault();
		this.props.onCancel();
	}

});

module.exports = EditForm;
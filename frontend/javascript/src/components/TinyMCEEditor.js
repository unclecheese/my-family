/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react');

module.exports = React.createClass({displayName: 'TinyMCEEditor',

    _init: function () {
        tinymce.init({
            menubar: false,
            content_css: document.querySelector("base").href + "frontend/css/tinymce.css",
            selector: "textarea.wysiwyg",
            plugins: [
                "paste"
            ],
            paste_auto_cleanup_on_paste : true,
            paste_remove_styles: true,
            paste_remove_styles_if_webkit: true,
            paste_strip_class_attributes: true, 
            paste_retain_style_properties: "font-size, font-style, color",   
            toolbar: "bold italic | alignleft aligncenter | bullist numlist | paste"
        });                
    },

    componentDidMount: function () {
        this._init();
    },

    componentWillUnmount: function () {
        tinyMCE.get(this.props.id).remove();
    },

    componentWillReceiveProps: function () {
        this._init();
    },

    getValue: function () {        
        return tinyMCE.get(this.props.id).getContent();
    },

    render: function () {
        return (
            this.transferPropsTo(
                <textarea id={this.props.id} className="wysiwyg"></textarea>
            )
        );
    }
});
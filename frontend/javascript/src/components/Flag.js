/**
 * @jsx React.DOM
 */
var React = require('react/addons');
var BS = require('react-bootstrap');

module.exports = React.createClass({displayName: 'Flag',

	_getOptions: function () {		
		if(this.props.isOwner) {
			return [
		        <BS.MenuItem>Actions...</BS.MenuItem>,
		        <BS.MenuItem divider />,
				<BS.MenuItem eventKey="edit">Edit this post</BS.MenuItem>,
		        <BS.MenuItem eventKey="delete">Delete this post</BS.MenuItem>,
			];
		}

		return [
	        <BS.MenuItem>Flag this post as...</BS.MenuItem>,
	        <BS.MenuItem divider />,
	        <BS.MenuItem eventKey="dusoism">A Dusoism</BS.MenuItem>,
	        <BS.MenuItem eventKey="shadows">From the shadows</BS.MenuItem>,
	        <BS.MenuItem eventKey="robson">Illiciting a Robson defense</BS.MenuItem>,
	        <BS.MenuItem eventKey="carlino">Gross exaggeration of what actually happened</BS.MenuItem>,
	        <BS.MenuItem eventKey="steve">Oh, Steve...</BS.MenuItem>
		];

	},


	render: function () {
		var glyph = this.props.isOwner ? 'cog' : 'flag';
	    return (
	    <div className="reply-flag">
	      <BS.DropdownButton onSelect={this.props.onSelect}  bsStyle='link' title={<BS.Glyphicon glyph={glyph} />}>
	      	{this._getOptions()}
	      </BS.DropdownButton>
	    </div>         
	    );
	}
});
/**
 * @jsx React.DOM
 */
var React = require('react');
var BS = require('react-bootstrap');
var Router = require('react-router');
var req = require('reqwest');
var urls = require('../utils/urlSegmentGenerator');

module.exports = React.createClass({displayName: 'Navigation',

	mixins: [Router.Navigation, Router.State],

	goRandom: function (e) {
		e.preventDefault();
		req({
			url: 'api/v1/random',
			type: 'json'
		})
		.then(function(id) {			
			this.transitionTo('/'+urls.generateUrlSegment()+'/'+id);
		}.bind(this));
		
	},

	goCreate: function (e) {
		e.preventDefault();
		this.transitionTo('/new');
	},

	goHome: function (e) {
		e.preventDefault();
		this.transitionTo('/');
	},



	render: function () {
		var brand = <div onClick={this.goHome}>
						<span className="desktop">
							{"Balls n' Nuts"}
						</span>
						<span className="mobile">
							{"BnN"}
						</span>
					</div>;

		return (
			    <BS.Navbar className="swatch-red-white" fluid fixedTop brand={brand}>
			      <BS.Nav className="navbar-right">
			      	<BS.Button onClick={this.goRandom} className="random-post nav-btn" navItem>Random post</BS.Button>
			      	<BS.Button onClick={this.goCreate} className="new-post nav-btn" navItem>New post</BS.Button>			      	
			      	<li className="hamburger"><a onClick={this.props.onToggleMobile}><BS.Glyphicon glyph="align-justify" /></a></li>
			      </BS.Nav>
			    </BS.Navbar>			

		);
	}
});

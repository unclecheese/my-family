/**
 * @jsx React.DOM
 */
var React = require('react/addons');
var BS = require('react-bootstrap');
var Flags = require('../utils/flags');

module.exports = React.createClass({displayName: 'FlagList',


	getDefaultProps: function () {
		return {
			flags: []
		};
	},

	
	render: function () {
	    if(!this.props.flags.length) return <div />;
	    
	    return (                
          <ul className="reply-flags">
          {this.props.flags.map(function(flag, i) {
          	  var obj = Flags[flag.flag];
              return <li key={i}>
                        Flagged as <span className={"label label-"+obj.style}>{obj.label}</span> by {flag.member} on {flag.date}
                      </li>
          })}
          </ul>                
	    );
	}
});
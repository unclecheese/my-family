/**
 * @jsx React.DOM
 */
var React = require('react');
var Router = require('react-router');
var SearchForm = require('./SearchForm');
var FilterForm = require('./FilterForm');
var BS = require('react-bootstrap');
var Link = Router.Link;
var ThreadStore = require('../stores/ThreadStore');
var ThreadActions = require('../actions/ThreadActions');
var urls = require('../utils/urlSegmentGenerator');

module.exports = React.createClass({displayName: 'sidebar',

    mixins: [Router.Navigation, Router.State],

    getInitialState: function () {
        return {
            threads: []
        };
    },

    componentDidMount: function () {
        ThreadStore.on('change', this._onChange);
        ThreadActions.getPopularThreads();
    },

    componentWillUnmount: function() {
        ThreadStore.removeListener('change', this._onChange);
    },

    searchHandler: function (value) {
        if(value.match(/[A-Za-z0-9]+/)) {
            this.transitionTo('/', {}, {q: encodeURI(value)});
        }
        else {
            this.transitionTo('/');
        }
    },


    filterHandler: function (start) {
        this.transitionTo('/', {}, {startDate: start});
    },


    flagHandler: function (e) {
        this.transitionTo('flag', {type: e.target.value});
    },

    render: function () {
    	var mobileClass = this.props.mobileActive ? 'open' : 'closed';
        return (
                    <div className={"col-xs-12 col-md-4 fixed-sidebar " + mobileClass}>
                                <SearchForm onUpdate={this.searchHandler} searchTerm={this.props.searchTerm} />
                                <BS.Row className="sidebar-tab-header">
                                    <BS.Col xs={12} md={6} className="hot-searches-header">
                                        <h5 className="active">Browse by month</h5>
                                    </BS.Col>
                                </BS.Row>
                                <FilterForm onUpdate={this.filterHandler} startDate={this.props.startDate} />
                                <BS.Row className="sidebar-tab-header">
                                    <BS.Col xs={12} md={6} className="hot-searches-header">
                                        <h5 className="active">Flagged posts</h5>
                                    </BS.Col>
                                </BS.Row>
                                <BS.Row className="flag-filter">
                                    <BS.Col xs={12}>
                                        <div className="select-wrapper">
                                            <select onChange={this.flagHandler} className="form-control" value={this.props.flag}>
                                                <option value=""> </option>
                                                <option value="dusoism">Dusoisms</option>
                                                <option value="shadows">From the shadows</option>
                                                <option value="robson">Robson defense</option>
                                                <option value="carlino">Gross exaggeration of what actually happened</option>
                                                <option value="steve">Oh, Steve...</option>
                                            </select>
                                            <i className="glyphicon glyphicon-chevron-down down" />
                                        </div>
                                    </BS.Col>
                                </BS.Row>


                            <BS.Row className="sidebar-tab-header">
                                <BS.Col xs={12} md={6} className="hot-searches-header">
                                    <h5 className="active">Hot Searches</h5>
                                </BS.Col>
                            </BS.Row>
                            <BS.Row>
                                <BS.Col lg={12} className="hot-searches">
                                    <ol>
                                        <li><Link to="home" query={{preset: 1}}>Legendary posts</Link></li>
                                        <li><Link to="home" query={{preset: 2}}>Longest rants</Link></li>
                                        <li><Link to="home" query={{preset: 3}}>Most discussed</Link></li>
                                        <li><Link to="home" query={{preset: 4}}>Most profane</Link></li>
                                        <li><Link to="home" query={{preset: 5}}>Posts by Steve that no one ever answered</Link></li>
                                        <li><Link to="home" query={{preset: 6}}>Posts by Steve called "huh"</Link></li>
                                    </ol>
                                </BS.Col>
                            </BS.Row>
                            <BS.Row>

                                <BS.Col xs={12} md={6} mdOffset={3} className="account">
                                    <Link to="account">My Account</Link> | <a href="Security/logout">Logout</a>
                                </BS.Col>
                            </BS.Row>

                    </div>
        );

    },

    _onChange: function () {        
        this.setState({
            threads: ThreadStore.getState().popularThreads
        });
    }
		
});

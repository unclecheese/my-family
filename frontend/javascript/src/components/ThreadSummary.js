/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react');
var Router = require('react-router');
var urls = require('../utils/urlSegmentGenerator');

module.exports = React.createClass({displayName: 'ThreadSummary',

    mixins: [Router.Navigation, Router.State],

    clickHandler: function (e) {
        var word = urls.generateUrlSegment();
        this.transitionTo('/'+word+'/'+this.props.id, {}, this.getQuery());
    },

	render: function() {		
		return (
            <li className="col-md-6 post-item" onClick={this.clickHandler}>

                <div className="grid-post">                                


                <div className="post-meta">                             
                    <small className="post-author">
                        <span>{this.props.author}</span>
                    </small>
                    <small className="post-date">
                        Updated: <span className="lastedited">{this.props.lastEdited}</span>
                    </small>
                </div>
                    {(this.props.unreadPosts.length > 0) && <span className="unread"><strong>{this.props.unreadPosts.length}</strong> new </span>}
                    <article className="post post-showinfo">
                        <div className="post-body">
                            <p>{this.props.summary}</p>
                        </div>
                        <div className="post-head small-screen-center">
                            <h2 className="post-title">										      
                                <Router.Link to="thread" params={{threadID: this.props.id}} query={this.getQuery()}>
						        {this.props.title}										      
                                </Router.Link>
						    </h2>
                        </div>

                        <div className="bordered post-extras text-center">
                            <div className="text-center">
                                <span className="post-link">
							      <span>
							        <i className="fa fa-comments"></i>
							        {this.props.replies} replies
							      </span>
							    </span>
                            </div>
                        </div>
                    </article>
                </div>
                
            </li>

		);
	}
})
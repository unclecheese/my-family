/**
 * @jsx React.DOM
 */
var React = require('react');
var debounce = require('../utils/debounce');

module.exports = React.createClass({displayName: 'searchform',

    timeout: null,

    getInitialState: function () {
        return {
            search: this.props.searchTerm
        };
    },

    componentWillReceiveProps: function(nextProps) {
        if(nextProps.searchTerm != this.props.searchTerm) {
            this.setState({
                search: nextProps.searchTerm
            });
        }
    },

    handleUpdate: function () {
        var v = this.refs.search ? this.refs.search.getDOMNode().value : '';
        this.setState({
            search: v 
        });
        
        if(this.timeout) window.clearTimeout(this.timeout);

        this.timeout = window.setTimeout(function() {
            this.props.onUpdate(v);
        }.bind(this), 250);
    },

    render: function () {
        return (
        <form className="search-form">
                <input value={this.state.search} className="form-control" placeholder="search" ref="search" type="text" onChange={this.handleUpdate}/>
        </form>
        );        
    }
});
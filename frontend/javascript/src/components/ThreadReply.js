/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react/addons');
var BS = require('react-bootstrap');
var req = require('reqwest');
var Flag = require('./Flag');
var FlagList = require('./FlagList');
var FileList = require('./FileList');
var Router = require('react-router');
var EditForm = require('./EditForm');

module.exports = React.createClass({displayName: 'ThreadReply',

  getDefaultProps: function () {
    return {
      author: '',
      content: '',
      files: [],
      date: '',
      title: null
    }
  },

	render: function () {
		return (
            <li className="media media-comment" id={'post-'+this.props.id}>
              <div className="box-round box-mini pull-left">
                <div className="box-dummy" />
                <a className="box-inner" href="#">
                  <img alt className="media-objects img-circle" src={"http://www.gravatar.com/avatar/"+this.props.hash+"?d=identicon"} />
                </a>
              </div>
              <div className="media-body">
                <div className="media-inner">
                  <h5 className="media-heading clearfix">
                    {this.props.author}, {this.props.date} 
                    {this.props.title &&
                      <em>{" "}(From the thread{" "} 
                        <Router.Link to={"/thread/"+this.props.threadID}>
                        {'"'+this.props.title+'"'})
                        </Router.Link>
                      </em>
                    }
                  </h5> 
                  {!this.props.isEditing && 
                  	<div dangerouslySetInnerHTML={{__html: this.props.content }} />
              	  }
              	  {this.props.isEditing &&
              	  	<EditForm onSubmit={this.props.onEditPost} onCancel={this.props.onCancelEdit} post={{id: this.props.id, content: this.props.rawContent, threadID: this.props.threadID}} />
              	  }
                </div>
               </div>
               {this.props.files.length > 0 &&
                  <FileList files={this.props.files} />
               }
               <Flag isOwner={this.props.isOwner} onSelect={this.props.onFlag} /> 
               <FlagList flags={this.props.flags} /> 

            </li>
		);
	}
});

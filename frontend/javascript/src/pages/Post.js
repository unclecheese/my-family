/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react');
var ThreadActions = require('../actions/ThreadActions');
var Sidebar = require('../components/Sidebar');
var BS = require('react-bootstrap');
var Router = require('react-router');
var Dropzone = require('../components/Dropzone');
var DropzoneUploader = require('../mixins/DropzoneUploader');

module.exports = React.createClass({displayName: 'Post',

    mixins: [Router.Navigation, DropzoneUploader],


    getInitialState: function () {
        return {
            message: null,
            type: 'success'
        }
    },


    submitHandler: function (e) {
        e.preventDefault();
        var title = this.refs.title.getDOMNode().value;
        var content = this.refs.content.getDOMNode().value;

        ThreadActions.create(title, content, this.state.uploadedFiles);

        this.transitionTo('/');
    },


    render: function () {        
        return (
            <div className="col-xs-12 col-md-8 col-md-offset-2">
                {this.state.message &&
                    <BS.Alert bsStyle={this.state.type} dismissAfter={2000}>{this.state.message}</BS.Alert>
                }
                <h2>Create a post</h2>
                <form className="post-form" onSubmit={this.submitHandler}>
                    <div className="form-group">
                        <label>Title</label>
                        <input ref="title" type="text" className="form-control" autofocus/>
                    </div>
                    <div className="form-group">
                        <label>Content</label>                    
                        <textarea ref="content" rows={20} />
                    </div>

                    <Dropzone url='api/v1/upload' success={this.handleUpload} removedfile={this.handleRemove} addRemoveLinks />

                    <button type="submit" className="btn btn-primary btn-block">Submit post</button>
                </form>
            </div>
        );
    }
});
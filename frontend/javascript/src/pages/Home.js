/**
 * @jsx React.DOM
 */
// Stylesheet
var React = require('react/addons');
var ThreadSummary = require('../components/ThreadSummary');
var InfiniteScroll = require('react-infinite-scroll')(React);
var Sidebar = require('../components/Sidebar');
var Router = require('react-router');
var ThreadStore = require('../stores/ThreadStore');
var ThreadActions = require('../actions/ThreadActions');
var AppDispatcher = require('../dispatcher/dispatcher');
var moment = require('moment');

module.exports = React.createClass({displayName: 'Home',

	mixins: [Router.State],

	getInitialState: function () {		
		return {
			threads: [],			
			hasMore: false,
			resultsTitle: null
		};
	},

	componentWillReceiveProps: function (nextProps) {
		var q = this.getQuery();
		if(JSON.stringify(q) != JSON.stringify(this._params)) {
			this.refs.scroller.setPageLoaded(0);
			this._params = q;			
			this.setState({resultsTitle: this._createResultsTitle()});			
			this.loadThreads(0);
		}
	},

	loadThreads: function(num) {				
		this.currentPage = num;		
		ThreadActions.get(this._params, this.currentPage);		
	},

	componentWillMount: function () {
		ThreadStore.on('change', this._onChange);

		this._params = this.getQuery() || {};
		this.currentPage = -1;
		this.setState({resultsTitle: this._createResultsTitle()});						
		this.loadThreads(0);
	},

	componentWillUnmount: function () {
		ThreadStore.removeListener('change', this._onChange);
	},

	_createResultsTitle: function () {
		var p = this._params;
		if(p.q) return 'Search for "'+decodeURI(p.q)+'"';
		if(p.startDate && p.endDate) return 'Showing posts between ' + moment(p.startDate).format('MMMM YYYY') + ' and ' + moment(p.endDate).format('MMM YYYY');
		if(p.startDate) return 'Showing posts from ' + moment(p.startDate).format('MMMM YYYY');
		if(p.endDate) return 'Showing posts up until ' + moment(p.endDate).format('MMMM YYYY');
		if(p.preset == 1) return 'Legendary posts';
		if(p.preset == 2) return 'Longest rants';
		if(p.preset == 3) return 'Most discussed posts';
		if(p.preset == 4) return 'Most profane posts';
		if(p.preset == 5) return 'Posts by Steve that no one ever answered';
		if(p.preset == 6) return 'Posts by Steve called "huh"';

		return 'Showing all posts';
	},

	render: function () {		
		return (
	    <div className="col-xs-12 col-md-8">	 
	    	{this.state.resultsTitle && 
	    	<h3>{this.state.resultsTitle}</h3>   
	    	}
	       <ul className="list-unstyled isotope no-transition">
			<InfiniteScroll 
				ref='scroller'
				pageStart={this.currentPage} 
				loadMore={this.loadThreads} 
				hasMore={this.state.hasMore} 
				loader={<div className="loader">Loading ...</div>}>
					{this.state.threads.map(function(thread) {
						return (
							<ThreadSummary
								key={thread.id}
								id={thread.id}
								title={thread.title} 
								summary={thread.summary} 
								author={thread.author} 
								date={thread.date}
								lastEdited={thread.lastEdited}
								unreadPosts={thread.unreadPosts}
								ago={thread.ago}								
								replies={thread.replies} />
						);
					})}
			</InfiniteScroll>
			</ul>
		</div>
		);
		
	},

	_onChange: function () {				
		this.setState({
			threads: ThreadStore.getState().threads,
			hasMore: ThreadStore.getState().hasMore
		});
	}
})
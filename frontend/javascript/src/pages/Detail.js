/**
 * @jsx React.DOM
 */
var React = require('react/addons');
var Router = require('react-router');
var Post = require('../components/Post');
var Sidebar = require('../components/Sidebar');
var ThreadReply = require('../components/ThreadReply');
var BS = require('react-bootstrap');
var PostStore = require('../stores/PostStore');
var Flag = require('../components/Flag');
var Star = require('../components/Star');
var FlagList = require('../components/FlagList');
var PostActions = require('../actions/PostActions');
var ThreadActions = require('../actions/ThreadActions');
var Dropzone = require('../components/Dropzone');
var DropzoneUploader = require('../mixins/DropzoneUploader');
var FileList = require('../components/FileList');
var scrollToElement = require('../utils/scrollToElement');
var EditForm = require('../components/EditForm');

module.exports = React.createClass({displayName: 'Detail',

	mixins: [Router.State, Router.Navigation, DropzoneUploader],

	getInitialState: function () {
		return {
			main: null,
			replies: [],
			title: '',
      		isFavorited: false,
      		favorites: [],
      		editingPost: null
		};
	},

	componentWillReceiveProps: function (nextProps) {
    PostActions.reset();    
		PostActions.get(this.getParams().threadID);
    setTimeout(function() {
      PostActions.markAsRead(this.getParams().threadID);
    }.bind(this), 1000);
	},

  componentWillMount: function () {    
    PostStore.on('change', this._onChange);   
    PostActions.reset();
    PostActions.get(this.getParams().threadID);
    setTimeout(function() {
      PostActions.markAsRead(this.getParams().threadID);
    }.bind(this), 1000);
  },


  componentDidUpdate: function(prevProps, prevState) {
  	if(!prevState.replies.length && this.state.replies.length) {
  		var firstUnread, div;
  		this.state.replies.some(function(reply) {
  			if(reply.isUnread) {
  				firstUnread = reply;
  				return true;
  			}
  		})
  		if(firstUnread) {
			div = document.getElementById('post-'+firstUnread.id);  			
			scrollToElement(div, 300, 60);
  		}  		
  	}  	
  },

  componentWillUnmount: function () {    
    PostStore.removeListener('change', this._onChange);
  },

	handlePost: function (e) {
		e.preventDefault();		
		var content = this.refs.postcontent.getDOMNode().value;
    
    PostActions.create(this.getParams().threadID, content, this.state.uploadedFiles);

    this.refs.postcontent.getDOMNode().value = '';
	},

  createFlagHandler: function (reply) {
    return function (key) {
      if(key === 'edit') {
      	this.setState({
      		editingPost: reply.id
      	});
      }
      else if(key === 'delete') {
      	PostActions.destroy(reply.id, this.getParams().threadID);
      }
      else {
    	PostActions.flag(reply.id, this.getParams().threadID, key);
  	  }
    }.bind(this);
  },

  favoriteThread: function () {
    ThreadActions.favorite(this.getParams().threadID);
  },

  goHome: function () {
    this.transitionTo('/', {}, this.getQuery());
  },

  render: function () {
  	var main = this.state.main;
    
    if(!main) return <div className="loader" />;
    
    return (
	    <div className="col-xs-12 col-md-7">

        <article className="post post-showinfo detail">
          <button className='btn back-button' onClick={this.goHome}>
            <i className="glyphicon glyphicon-chevron-left" />
          </button>

          <div className="post-head small-screen-center">
          <div className="post-meta">
            <small className="post-author">
              <span>{this.state.main.author}</span>
            </small>
            <small className="post-date">
              <span>{this.state.main.date}</span>
            </small>
            {this.state.favorites.length > 0 &&
              <small className="post-favorites">
                {this.state.favorites.map(function (f) {
                   return <div><i className="glyphicon glyphicon-star" /> Favorited by {f.name} on {f.date}</div> 
                })}
              </small>
            }

          </div>

            <h2 className="post-title">
              <span>
                {this.state.title}
              </span>
            </h2>
            <Flag isOwner={this.state.main.isOwner} onSelect={this.createFlagHandler(this.state.main)} />
            <Star onSelect={this.favoriteThread} selected={this.state.isFavorited} />
          </div>

          <div className="post-body">
	      {this.state.editingPost !== this.state.main.id &&           	
          	<div dangerouslySetInnerHTML={{__html: this.state.main.content}} />
          }
          {this.state.editingPost == this.state.main.id && 
	      	  <EditForm 
	      	  	onSubmit={this._onEditPost} 
	      	  	onCancel={this._onCancelEdit} 
	      	  	post={{id: main.id, content: main.rawContent, threadID: main.threadID}} />
          }
          </div>
          <FlagList flags={this.state.main.flags} />
          {this.state.main.files.length > 0 &&
            <FileList files={this.state.main.files} />
          }
        </article>
        <div className="comments padded post-showinfo" id="comments">
          <div className="comments-head small-screen-center">
            <h3>
              {this.state.replies.length} replies
            </h3>
          </div>
          <ul className="comments-list comments-body media-list">
          	{this.state.replies.map(function(reply) {          		
          		return <ThreadReply 
                        onFlag={this.createFlagHandler(reply)} 
                        author={reply.author} 
                        content={reply.content}
                        rawContent={reply.rawContent} 
                        date={reply.date} 
                        key={reply.id} 
                        id={reply.id}
                        threadID={reply.threadID}
                        flags={reply.flags}
                        files={reply.files}
                        isOwner={reply.isOwner}
                        isEditing={this.state.editingPost === reply.id}
                        onEditPost={this._onEditPost}
                        onCancelEdit={this._onCancelEdit}
                        hash={reply.hash} />
          	}.bind(this))}
          </ul>
        </div>
        <div className="comments-form post-showinfo">
          <form className="comments-body" onSubmit={this.handlePost}>
            <fieldset>
              <div className="form-group form-icon-group">
                <textarea ref="postcontent" className="form-control" id="message" placeholder="Write a response" rows={15} />                
              </div>
              <Dropzone url='api/v1/upload' success={this.handleUpload} removedfile={this.handleRemove} addRemoveLinks />

              <div className="form-group small-screen-center">
                <button className="btn btn-primary btn-icon btn-icon-right btn-block" type="submit">
                  Reply
                </button>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    );
  },

  _onChange: function () {    
    this.setState(PostStore.getState());
  },


  _onEditPost: function (postID, threadID, content) {
  	this.setState({
  		editingPost: null
  	});
  },

  _onCancelEdit: function (e) {
  	this.setState({
  		editingPost: null
  	});
  }

});
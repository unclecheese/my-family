/**
 * @jsx React.DOM
 */
var React = require('react/addons');
var Router = require('react-router');
var Post = require('../components/Post');
var Sidebar = require('../components/Sidebar');
var ThreadReply = require('../components/ThreadReply');
var BS = require('react-bootstrap');
var PostStore = require('../stores/PostStore');
var Flag = require('../components/Flag');
var Star = require('../components/Star');
var FlagList = require('../components/FlagList');
var PostActions = require('../actions/PostActions');
var ThreadActions = require('../actions/ThreadActions');
var Dropzone = require('../components/Dropzone');
var DropzoneUploader = require('../mixins/DropzoneUploader');
var FileList = require('../components/FileList');
var Flags = require('../utils/flags');

module.exports = React.createClass({displayName: 'Detail',

	mixins: [Router.State, Router.Navigation, DropzoneUploader],

	getInitialState: function () {
		return {
      main: null,
			replies: [],
			title: this.getParams().type
		};
	},


  componentWillMount: function () {    
    PostStore.on('change', this._onChange);   
    PostActions.reset();
    PostActions.getByFlag(this.getParams().type);
  },


  componentWillUnmount: function () {    
    PostStore.removeListener('change', this._onChange);
  },

  componentWillReceiveProps: function () {
    PostActions.getByFlag(this.getParams().type);
  },


  goHome: function () {
    this.transitionTo('/');
  },


	render: function () {
    if(!this.state.main) return <div className="loader" />;
    var replies = this.state.replies;
    replies.unshift(this.state.main);

    return (
	    <div className="col-xs-12 col-md-7">
          <h2>Posts marked {'"'+Flags[this.getParams().type].label+'"'}</h2>
          <ul className="comments-list comments-body media-list">
          	{replies.map(function(reply) {
          		return <ThreadReply               
                        title={reply.threadTitle}
                        threadID={reply.threadID}
                        author={reply.author} 
                        content={reply.content} 
                        date={reply.date} 
                        key={reply.id} 
                        flags={reply.flags}
                        files={reply.files}
                        hash={reply.hash} />
          	}.bind(this))}
          </ul>
      </div>
    );
  },

  _onChange: function () {    
    this.setState(PostStore.getState());
  }

});
/**
 * @jsx React.DOM
 */
var React = require('react');
var Router = require('react-router');
var req = require('reqwest');
var BS = require('react-bootstrap');

module.exports = React.createClass({displayName: 'Account',

	getInitialState: function () {
		return {
			message: null,
			type: 'success',
			name: this.props.name,
			email: this.props.email,
			notifications: this.props.notifications,
			period: this.props.period,
			token: null,
			changingPassword: false
		};
	},


	componentWillMount: function() {
		req({
			url: 'api/v1/account',
            type: 'json',
            method: 'GET'
		})
		.then(function (json) {
			this.setState(json);
		}.bind(this))
        .fail(function(json) {
            this.setState({
                message: json.response,
                type: 'danger'
            })
        }.bind(this));

	},

	handleName: function (e) {
		this.setState({
			name: e.target.getDOMNode().value
		});
	},

	handleEmail: function (e) {
		this.setState({
			email: e.target.getDOMNode().value
		});
	},

	handleNotifications: function (e) {
		this.setState({
			notifications: e.target.checked
		});
	},

	handlePeriod: function (e) {
		this.setState({
			period: e.target.value
		})
	},

	handlePrefs: function (e) {		
		if(e.target.checked) {			
			this.setState({
				prefs: e.target.value
			});
		}
	},

	handleSubmit: function (e) {		
		e.preventDefault();
		var pass, confirm;
		if(this.state.changingPassword) {
			pass = this.refs.password.getDOMNode().value;
			confirm = this.refs.password_confirm.getDOMNode().value;
		}
		var data = {
			Name: this.state.name,
			Email: this.state.email,
			EmailNotifications: this.state.notifications,
			Period: this.state.period
		};

		if(pass || confirm) {
			if(pass !== confirm) {
				this.setState({
					message: 'Passwords don\'t match.',
					type: 'danger'
				});

				return;
			}
			data.Password = pass;
		}

        req({
            url: 'api/v1/account',
            type: 'json',
            method: 'PUT',
            data: data
        })
        .then(function(json) {          
            this.setState({
                message: json.response,
                type: 'success'
            })
        }.bind(this))
        .fail(function(json) {
            this.setState({
                message: json.response,
                type: 'danger'
            })
        }.bind(this));

	},


	togglePassword: function (e) {
		e.preventDefault();
		this.setState({
			changingPassword: !this.state.changingPassword
		});
	},


    render: function () {
        return (
            <div className="col-xs-12 col-md-8 col-md-offset-2">
                {this.state.message &&
                    <BS.Alert bsStyle={this.state.type} dismissAfter={2000}>{this.state.message}</BS.Alert>
                }                
                <form className="post-form" onSubmit={this.handleSubmit}>
                	<h4>Your info</h4>
                    <div className="form-group">
                        <label>Name</label>
                        <input value={this.state.name} ref="name" type="text" onChange={this.handleName} className="form-control" autofocus/>
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input value={this.state.email} ref="email" type="email" onChange={this.handleEmail} className="form-control" />
                    </div>

                    <div className="form-group">
                    	<label>Your auto-login URL</label>
                    	<a style={{fontSize: '18px'}} className="form-control" href={'http://ballsandnuts.com/login?k='+this.state.token}>{'http://ballsandnuts.com/login?k='+this.state.token}</a>                    	
                    </div>

                    <button bsStyle='primary' onClick={this.togglePassword}>Change password</button>

                    {this.state.changingPassword && 
                    <div>
	                    <div className="form-group">
	                        <label>New Password</label>
	                        <input ref="password" type="password" className="form-control" />
	                    </div>

	                    <div className="form-group">
	                        <label>Confirm New Password</label>
	                        <input ref="password_confirm" type="password" className="form-control" />
	                    </div>
	                </div>
                	}
					<h4>Email Notification</h4>
					<div className="checkbox">
						<label>
							<input onChange={this.handleNotifications} type="checkbox" name="notifications" value="1" checked={this.state.notifications} />
							{"Email me updates when I haven't been on in a while"}
						</label>
					</div>
					{this.state.notifications && 
                    <div className="form-group">
                        <label>After how many days?</label>
                        <select className="form-control" onChange={this.handlePeriod} value={this.state.period}>
                        	<option value={1}>1</option>
                        	<option value={2}>2</option>
                        	<option value={3}>3</option>
                        	<option value={4}>4</option>
                        	<option value={5}>5</option>
                        	<option value={6}>6</option>
                        	<option value={7}>7</option>
                        	<option value={8}>8</option>
                        	<option value={9}>9</option>
                        	<option value={10}>10</option>
                        	<option value={14}>14</option>
                        	<option value={21}>21</option>
                        	<option value={30}>30</option>
                        </select>
                    </div>
                    }
					<div className="row">
                    	<div className="col-xs-12 col-md-6">
                    		<button type="submit" className="btn btn-primary btn-block">Save changes</button>
                    	</div>
                    	<div className="col-xs-12 col-md-6">
                    		<a href="/" className="btn btn-default btn-block">Cancel</a>
                    	</div>
                    </div>

                </form>
            </div>
        );
    }
})
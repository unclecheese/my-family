/**
 * @jsx React.DOM
 */
var React = require('react');
var Router = require('react-router');
var Sidebar = require('../components/Sidebar');

module.exports = React.createClass({displayName: 'Main',

	mixins: [Router.State],

	getInitialState: function () {
		return {
			startDate: this.getQuery().startDate || null,
			endDate: this.getQuery().endDate || null,
			q: this.getQuery().q || '',
			flag: this.getParams().type || ''
		};
	},

	componentWillReceiveProps: function (nextProps) {		
		this.setState(this.getInitialState());
	},

	render: function () {
		return (
		<div>
			<Router.RouteHandler />
			<Sidebar 
				mobileActive={this.props.mobileActive}
				startDate={this.state.startDate} 
				endDate={this.state.endDate} 
				flag={this.state.flag}
				searchTerm={decodeURI(this.state.q || '')} />

		</div>
		);		
	}
})
/**
 * @jsx React.DOM
 */
var React = require('react');
var Router = require('react-router');

module.exports = React.createClass({displayName: 'NotFound',

    render: function () {
        return (
            <div className="col-xs-12 col-md-8 col-md-offset-2">
            	<h2>Not found!</h2>
            </div>
        );
    }
})
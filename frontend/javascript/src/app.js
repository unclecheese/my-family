/**
 * @jsx React.DOM
 */
var React = require('react');
var BS = require('react-bootstrap');
var Navigation = require('./components/Navigation');
var Router = require('react-router');

module.exports = React.createClass({

	getInitialState: function () {
		return {
			errorMsg: false,
			mobileActive: false
		};
	},

	handleDismiss: function () {
		this.setState({
			errorMsg: false
		});
	},
	
	toggleMobile: function (e) {
		this.setState({
			mobileActive: !this.state.mobileActive
		});
	},


	render: function () {
		return (
			<div>

				{this.state.errorMsg && 
					<BS.Alert bsStyle="warning" onDismiss={this.handleDismiss}>
					      {this.state.errorMsg}
					</BS.Alert>
				}
				<Navigation onToggleMobile={this.toggleMobile} />
		        <div id="content" role="main">
		            <section className="section swatch-white-red has-top">
		                <div className="decor-top">
		                    {"Balls n' Nuts"}
		                </div>
		                <div className="container">
		                    <div className="row">		                    
		                    	<Router.RouteHandler mobileActive={this.state.mobileActive} />
		                    </div>
		                </div>
		            </section>
		        </div>
			</div>
		);
	}
})
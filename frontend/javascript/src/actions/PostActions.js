var AppDispatcher = require('../dispatcher/dispatcher');
var API = require('../utils/api');
var createEndFunction = require('../utils/createEndFunction');

var PostActions = {

  /**
   * @param  {string} text
   */
  create: function(threadID, content, uploadedFiles) {
    var key = 'CREATE_POST';
    var params = {threadID: threadID, content: content, files: uploadedFiles};
    
    AppDispatcher.handleViewAction({
      actionType: key,
      params: params
    });

    API.createPost(threadID, content, uploadedFiles).end(function() {
      API.getPostsForThread(threadID).end(
        createEndFunction(key, params)
      )
    });

  },

  get: function(threadID) {
    var key = 'GET_POSTS';
    var params = {threadID: threadID};
    AppDispatcher.handleViewAction({
      actionType: key,
      params: params
    });
    
    API.getPostsForThread(threadID).end(
      createEndFunction(key, params)
    );

  },


  getByFlag: function (flag) {
    var key = 'GET_POSTS';
    var params = {flag: flag};
    AppDispatcher.handleViewAction({
      actionType: key,
      params: params
    });
    
    API.getPostsByFlag(flag).end(
      createEndFunction(key, params)
    );
  },

  markAsRead: function (threadID) {
    var key = 'MARK_AS_READ';
    var params = {threadID: threadID};
    AppDispatcher.handleViewAction({
      actionType: key,
      params: params
    });

    API.markAsRead(threadID).end(
      createEndFunction(key, params)
    );

  },

  flag: function (postID, threadID, flag) {
    var key = 'FLAG_POST';
    var params = {postID: postID, threadID: threadID, flag: flag};

    AppDispatcher.handleViewAction({
      actionType: key,        
      params: params
    });

    API.flagPost(postID, threadID, flag).end(function() {
      API.getPostsForThread(threadID).end(
        createEndFunction(key, params)
      )
    });
  },

  edit: function (postID, threadID, content) {
    var key = 'EDIT_POST';
    var params = {postID: postID, threadID: threadID, content: content};

    AppDispatcher.handleViewAction({
      actionType: key,        
      params: params
    });

    API.editPost(postID, threadID, content).end(function() {
      API.getPostsForThread(threadID).end(
        createEndFunction(key, params)
      )
    });
  },

  destroy: function (postID, threadID, flag) {
    var key = 'DELETE_POST';
    var params = {postID: postID, threadID: threadID};

    AppDispatcher.handleViewAction({
      actionType: key,        
      params: params
    });

    API.deletePost(postID, threadID).end(function() {
      API.getPostsForThread(threadID).end(
        createEndFunction(key, params)
      )
    });
  },

  reset: function () {
    var key = 'RESET_POSTS';

    AppDispatcher.handleViewAction({
      actionType: key,
      params: {}
    });
  }

};
module.exports = PostActions;

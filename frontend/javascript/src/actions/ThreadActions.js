var AppDispatcher = require('../dispatcher/dispatcher');
var API = require('../utils/api');
var TIMEOUT = 10000;
var createEndFunction = require('../utils/createEndFunction');


var ThreadActions = {

  /**
   * @param  {string} text
   */
  create: function(title, content, files) {
    var key = 'CREATE_THREAD';
    var params = {title: title, content: content, files: files};
    
    AppDispatcher.handleViewAction({
      actionType: key,
      params: params
    });

    API.createThread(title, content, files).end(
      createEndFunction(key, params)
    );

  },

  get: function(params, page) {
    var key = 'GET_ALL_THREADS';        
    params = params || {};

    AppDispatcher.handleViewAction({
      actionType: key,          
      params: params
    });
    
    API.getAllThreads(params, page).end(
      createEndFunction(key, params)
    );    


  },

  favorite: function (threadID) {
    var key = 'FAVORITE_THREAD';
    var params = {threadID: threadID}

    AppDispatcher.handleViewAction({
      actionType: key,      
      params: params
    });

    API.favoriteThread(threadID).end(function() {
      API.getPopularThreads().end(function(err, res) {
        params.popularThreads = res.body;
        API.getPostsForThread(threadID).end(
          createEndFunction(key, params)
        )
      })      
    });
  },

  getPopularThreads: function () {
    var key = 'GET_POPULAR_THREADS';
    
    API.getPopularThreads().end(
      createEndFunction(key, {})
    );
  }

};


module.exports = ThreadActions;

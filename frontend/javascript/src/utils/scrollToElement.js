module.exports = function (el, scrollDuration, padding) {
	scrollDuration = scrollDuration || 300;
	padding = padding || 0;

	if(typeof el === "string") {
		el = document.querySelector(selector);
	}

	if(!el) return;

	var interval = 10;
	requestAnimationFrame(step);

	function step () {
		setTimeout(function() {			
			if(scrollDuration < 1) {
				return;
			}
			var scrollPos = window.scrollY,
				offset = el.getBoundingClientRect().top,
				togo = offset - padding,
				clicksRemaining = scrollDuration/interval,
				stepSize = togo/clicksRemaining,
				nextY;

			if(Math.round(window.scrollY)  == padding) {
				return;
			}
			if(offset - stepSize < padding) {
				stepSize = offset - padding;
			}

			nextY = scrollPos+stepSize;
			window.scrollTo( 0, nextY );

			scrollDuration -= interval;

			requestAnimationFrame(step);
		}, interval );
	}
}

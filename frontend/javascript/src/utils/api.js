var request = require('superagent');
var assign = require('object-assign');

var API_URL = '/api/v1';
var TIMEOUT = 10000;

var _pendingRequests = {};


function abortPendingRequests(key) {
    if (_pendingRequests[key]) {
        _pendingRequests[key]._callback = function(){};
        _pendingRequests[key].abort();
        _pendingRequests[key] = null;
    }
}


function makeUrl(part) {
    return API_URL + part;
}


// a get request with an authtoken param
function get(url, data) {     
    return request
        .get(url)
        .query(data || {})
        .timeout(TIMEOUT);       
}

function post(url, data) {
    return request        
        .post(url)
        .type('form')
        .send(data || {})
        .timeout(TIMEOUT);
}

function put(url, data) {
    return request        
        .put(url)
        .type('form')
        .send(data || {})
        .timeout(TIMEOUT);
}

function del(url, data) {
	return request
		.del(url)
		.type('form')
		.send(data || {})
		.timeout(TIMEOUT);
}


var Api = {


    getAllThreads: function(data, page) {        
        var url = makeUrl('/threads');
        var key = 'GET_ALL_THREADS';
        data = assign({}, data, {page: page});
        abortPendingRequests(key);                
        return _pendingRequests[key] = get(url, data)
    },


    getPopularThreads: function () {
        var url = makeUrl('/popularthreads');
        var key = 'GET_POPULAR_THREADS';          
        abortPendingRequests(key);        
        
        return _pendingRequests[key] = get(url);
    },


    favoriteThread: function (threadID) {
        var url = makeUrl('/thread/'+threadID+'/favorite');
        var key = 'FAVORITE_THREAD';        
        abortPendingRequests(key);        
        
        return _pendingRequests[key] = post(url);
    },


    createThread: function (title, content, files) {
        var url = API_URL;
        var key = 'CREATE_THREAD';
        var params = {Title: title, Content: content, Files: files};
        abortPendingRequests(key);
        
        return _pendingRequests[key] = post(url, params);
    },


    getPostsForThread: function (threadID) {
        var url = makeUrl('/thread/'+threadID);
        var key = 'GET_POSTS';        
        abortPendingRequests(key);
        
        return _pendingRequests[key] = get(url);
    },

    getPostsByFlag: function (flag) {
        var url = makeUrl('/flag/'+flag);
        var key = 'GET_POSTS';        
        abortPendingRequests(key);
        
        return _pendingRequests[key] = get(url);
    },

    markAsRead: function (threadID) {
        var url = makeUrl('/thread/'+threadID+'/markasread');
        var key = 'MARK_AS_READ';        
        abortPendingRequests(key);
        
        return _pendingRequests[key] = post(url);        
    },


    createPost: function (threadID, content, files) {
        var url = makeUrl('/thread/'+threadID);
        var key = 'CREATE_POST';        
        abortPendingRequests(key);
        
        return _pendingRequests[key] = post(url, {
            Content: content,
            Files: files
        });
    },


    flagPost: function (postID, threadID, flag) {
        var url = makeUrl('/thread/'+threadID+'/post/'+postID+'/flag');
        var key = 'FLAG_POST';        
        abortPendingRequests(key);
        
        return _pendingRequests[key] = put(url, {Flag: flag});
    },

    editPost: function (postID, threadID, content) {
        var url = makeUrl('/thread/'+threadID+'/post/'+postID);
        var key = 'EDIT_POST';        
        abortPendingRequests(key);
        
        return _pendingRequests[key] = put(url, {Content: content});

    },

    deletePost: function (postID, threadID) {
        var url = makeUrl('/thread/'+threadID+'/post/'+postID);
        var key = 'DELETE_POST';        
        abortPendingRequests(key);
        
        return _pendingRequests[key] = del(url);
    }
    

};

module.exports = Api;
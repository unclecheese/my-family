var AppDispatcher = require('../dispatcher/dispatcher');
var TIMEOUT = 10000;

module.exports = function createEndFunction(key, params) {
  return function (err, res) {   
        if (err && err.timeout === TIMEOUT) {
            AppDispatcher.handleRequestAction({
              actionType: key,
              error: 'TIMEOUT',
              params: params
            });
        } else if (res.status === 400) {
            AppDispatcher.handleRequestAction({
              actionType: key,
              error: 'BAD_REQUEST',
              params: params
            });
        } else if (res.status === 403) {
          window.location.pathname='/Security/login?BackURL='+encodeURI(window.location.pathname)        
        } else if (!res.ok) {
            AppDispatcher.handleRequestAction({
              actionType: key,
              error: 'ERROR',
              params: params
            });
        } else {
            AppDispatcher.handleRequestAction({
              actionType: key,
              error: false,
              response: res.body,
              params: params
            });
        }      
    };
};
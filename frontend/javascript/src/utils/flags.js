module.exports = {
	'dusoism': {style: 'danger', label: 'Dusoism'},
	'shadows': {style: 'warning', label: 'From the shadows'},
	'robson': {style: 'primary', label: 'Robson defense'},
	'carlino': {style: 'info', label: 'Gross exaggeration'},
	'steve': {style: 'success', label: 'Oh, Steve...'}
	
}
module.exports = {

	getInitialState: function () {
		return {
			uploadedFiles: []
		}
	},


    handleUpload: function (file, response) {
        file.serverID = response;
        this.setState({
            uploadedFiles: this.state.uploadedFiles.concat([response])
        });

    },

    handleRemove: function (file) {
        var files = this.state.uploadedFiles;
        for(var i in files) {
            if(files[i] === file.serverID) {
                delete files[i];
                break;
            }
        }

        if (file.previewElement) {                                
            file.previewElement.parentNode.removeChild(file.previewElement);
        }

        this.setState({uploadedFiles: files});
    }
}
var AppDispatcher = require('../dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var _state = {
    threads: [],
    popularThreads: [],
    hasMore: true
};

var _latestParams = {};


var ThreadStore = assign({}, EventEmitter.prototype, {
    getState: function() {      
        return _state;
    },

    handleViewAction: function (action) {
      switch(action.actionType) {
        case 'GET_ALL_THREADS':
          if(JSON.stringify(action.params) !== JSON.stringify(_latestParams)) {
            _state.threads = [];
            _state.hasMore = false;        
            _latestParams = assign({}, action.params);     
          
            ThreadStore.emitChange();          
          }
          break;
      }

      return true;
    },


    handleRequestAction: function (action) {
      switch(action.actionType) {
          case 'CREATE_POST':
          case 'CREATE_THREAD':
            _state.threads = [];
            _state.hasMore = true;
            _latestParams: {};
            ThreadStore.emitChange();
            break;
          case 'GET_ALL_THREADS':          
              _state.threads = _state.threads.concat(action.response.results);              
              _state.hasMore = action.response.has_more;
              ThreadStore.emitChange();

            break;
          case 'FAVORITE_THREAD':
              _state.popularThreads = action.params.popularThreads.results;              
              ThreadStore.emitChange();
              break;
          case 'MARK_AS_READ':              
              _state.threads.every(function(thread, i) {
                if(thread.id == action.params.threadID) {
                  thread.isUnread = false;
                  ThreadStore.emitChange();

                  return false;
                }
              });
              break;
          case 'GET_POPULAR_THREADS':
              _state.popularThreads = action.response.results;  
              //_state.hasMore = false;          
              ThreadStore.emitChange();
            break;          

      }


      return true;
    },

    emitChange: function () {      
      this.emit('change');
    }
});



ThreadStore.appDispatch = AppDispatcher.register(function(payload) {    
    if(payload.source == 'VIEW_ACTION') {
      return ThreadStore.handleViewAction(payload.action);
    }
    else if(payload.source == 'REQUEST_ACTION') {
      return ThreadStore.handleRequestAction(payload.action);
    }
});

module.exports = ThreadStore;
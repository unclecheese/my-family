var AppDispatcher = require('../dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var PostActions = require('../actions/PostActions');

var _state = {
    main: null,
    title: '',
    replies: [],
    isFavorited: false
};

var PostStore = assign({}, EventEmitter.prototype, {
    getState: function() {
        return _state;
    },

    emitChange: function () {
      this.emit('change');
    },


    handleViewAction: function (action) {
      switch(action.actionType) {
          case 'CREATE_POST':
            _state.replies.push({content: action.params.content});
            PostStore.emitChange();
            break;
          case 'RESET_POSTS':         
              _state.main = null;
              _state.title = '',
              _state.replies = [];
              PostStore.emitChange();
            break;

          case 'FLAG_POST':
            break;
          case 'FAVORITE_THREAD':
              _state.isFavorited = !_state.isFavorited;
              PostStore.emitChange();
            break;
          case 'EDIT_POST':
          	  var post;
          	  if(action.params.postID === _state.main.id) {
          	  		post = _state.main
          	  }
          	  else {
	          	  _state.replies.some(function(reply) {
	          	  	if(reply.id === action.params.postID) {
	          	  		post = reply;

	          	  		return true;
	          	  	}
	          	  	return false;
	          	  });
          	 }

          	 if(post) {
      	  		post.content = action.params.content;
      	  		PostStore.emitChange();          	 	
          	 }
          	 break;
          case 'DELETE_POST':
              var index = null;
              if(action.params.postID === _state.main.id) {
              		window.location.pathname = '/';
              }
              else {
	          	  _state.replies.some(function(reply, i) {
	          	  	if(reply.id === action.params.postID) {
	          	  		index = i;

	          	  		return true;
	          	  	}
	          	  	return false;
	          	  });
	          	  if(index) {
	          	  	delete _state.replies[index];
	          	  	PostStore.emitChange();
	          	  }
          		}


      }

      return true;
    },


    handleRequestAction: function (action) {
      var results = action.response ? action.response.results : [];
      switch(action.actionType) {          
          case 'CREATE_POST':
              _state.main = results[0];
              _state.replies = results.slice(1);
              PostStore.emitChange();
            break;

          case 'GET_POSTS':
          case 'EDIT_POST':
          case 'DELETE_POST':
          case 'FAVORITE_THREAD':
              _state.main = results[0];    
              _state.replies = (results.length > 1) ? results.slice(1) : []
              _state.title = action.response.title;
              _state.isFavorited = action.response.isFavorited;
              _state.favorites = action.response.favorites;              
              PostStore.emitChange();
            break;
          case 'FLAG_POST':
            _state.main = results[0];
            _state.replies = (results.length > 1) ? results.slice(1) : [];
            PostStore.emitChange();
            break;
      }

      return true;
    }    
});



PostStore.appDispatch = AppDispatcher.register(function(payload) {
    if(payload.source == 'VIEW_ACTION') {
      return PostStore.handleViewAction(payload.action);
    }
    else if(payload.source == 'REQUEST_ACTION') {
      return PostStore.handleRequestAction(payload.action);
    }
});

module.exports = PostStore;
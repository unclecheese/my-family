<?php

class WeeklyEmailTask extends BuildTask {

	
	public function run($request) {		
		echo date('Y-m-d H:i:s').PHP_EOL;

		$week_ago = date('Y-m-d H:i:s', strtotime('-1 week'));
		$now = date('Y-m-d H:i:s');

		$weekPosts = Post::get()->filter(array(
			'Created:GreaterThan' => $week_ago			
		));

		echo $weekPosts->count() . " new posts ".PHP_EOL;

		$weekFlags = MarkedPost::get()->filter(array(
			'Created:GreaterThan' => $week_ago
		));

		echo $weekFlags->count() . " new flags ".PHP_EOL;

		$weekFavorites = FavoritedThread::get()->filter(array(
			'Created:GreaterThan' => $week_ago
		));

		echo $weekFavorites->count() . " new faves ".PHP_EOL;

		if(!$weekPosts->exists() && !$weekFlags->exists() && !$weekFavorites->exists()) {
			die('Nothing new');
		}
		$weekSent = 0;

		foreach(Member::get() as $m) {
			if($m->Email && $m->EmailPreference) {
				if($m->EmailPreference == 2) {
					$e = Email::create(
						'MyFamily <notifications@ballsandnuts.com>',
						$m->Email,
						'New stuff'
					)
					->setUserTemplate('digest')
					->populateTemplate(array(
						'Posts' => $weekPosts->exclude('AuthorID', $m->ID),
						'Flags' => $weekFlags->exclude('MemberID', $m->ID),
						'Faves' => $weekFavorites->exclude('MemberID', $m->ID)
					))
					->send();	
					echo "sent to {$m->getName()}".PHP_EOL;					
					$weekSent++;		

				}

			}
		}
		die();
	}
}
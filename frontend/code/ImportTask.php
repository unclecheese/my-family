<?php

use Symfony\Component\DomCrawler\Crawler;

class ImportTask extends CliController
{

	public function process() {	
		$fail = "";
		$files = glob(BASE_PATH.'/mysite/code/raw/*.html');
		$total = sizeof($files);
		$completed = 0;
		echo PHP_EOL;
		echo "Progress:        ";

		foreach($files as $file) {
			$thread = Thread::create();
			$html = file_get_contents($file);
			$dom = new Crawler($html);
			$title = $dom
						->filter('table.BodyBG table')->first()
							->filter('td.bb')->first()
							->text();

			
			$thread->Title = $title;
			$thread->write();
			$dom
				->filter("table.BodyBG table td[class='b'][valign='top'][width='100%']")->each(function($node) use($thread) {

					$post = Post::create();
					$post->Content = $node->html();

					$metaData = $node->parents()->filter('td.b')->eq(1);
					$text = $metaData->text();
					$ngh = explode('|', $text);
					$text = preg_replace('/[^A-Za-z0-9\-,]/','', $ngh[0]);

					
					preg_match('/[A-Z][a-z][a-z][0-9]+,20[0-9][0-9]/', $text, $matches);
					if(!empty($matches)) {
						$date = preg_replace_callback('/([A-Z][a-z][a-z])([0-9]+),(20[0-9][0-9])/', function ($result) {
							return date('Y-m-d H:i:s', strtotime($result[1] . " " . $result[2] . ", " . $result[3]));
						}, $matches[0]);
					}
					else {
						echo "NO DATE IN this\n\n$text\n\n";
						die();
					}


					$author = $metaData->filter('a')->first()->text();

					$post->Created = $date;
					$post->Author = $author;
					$post->ThreadID = $thread->ID;
					$post->write();
				});
			if($thread->Posts()->exists()) {
				$thread->Created = $thread->Posts()->sort('Created ASC')->first()->Created;
				$thread->write();
			}
			else {
				$fail .= "Thread {$thread->Title} has no posts!\n";
			}

			$completed++;

		    echo "\033[7D";
		    echo str_pad("{$completed}/{$total}", 7, ' ', STR_PAD_LEFT);

		}

		echo PHP_EOL,"Done.",PHP_EOL;
		echo $fail;
	}
}
<?php


class Thread extends DataObject
{

	private static $db = array (
		'Title' => 'Varchar(255)'
	);


	private static $has_many = array (
		'Posts' => 'Post',
		'Favorites' => 'FavoritedThread'
	);


	private static $belongs_many_many = array (
		'UnreadMembers' => 'Member'
	);

	private static $has_one = array (
		'FirstPost' => 'Post'
	);

	private static $extensions = array (
		"FulltextSearchable('Title')"
	);

	private static $searchable_fields = array (
		'Title',
		'PostContent'
	);

	public function getFavoriteForMember($member = null) {
		if(!$member) $member = Member::currentUser();

		return $this->Favorites()->filter('MemberID', $member->ID)->first();
	}


	public function isUnreadByMember($member = null) {
		return $this->getUnreadPostsForMember($member)->exists();
	}


	public function markAsRead($member = null) {
		if(!$member) $member = Member::currentUser();

		$member->UnreadPosts()->removeMany($this->Posts()->column('ID'));
	}


	public function getPostContent() {
		return implode(' ', $this->Posts()->column('Content'));
	}

	public function Link() {
		return Controller::join_links(Director::absoluteBaseURL(),'thread',$this->ID);
	}


	public function getUnreadPostsForMember($member = null) {
		if(!$member) $member = Member::currentUser();
		
		if($member->UnreadPosts()->exists()) {
			return $this->Posts()->byIDs($member->UnreadPosts()->column('ID'));	
		}

		return ArrayList::create();
		
	}
	
}
<?php

class LoginController extends Controller {

	public function init() {
		parent::init();

		if($this->request->getVar('k')) {
			$m = Member::get()->filter(array('Token' => $this->request->getVar('k')))->first();
			if($m) {
				$m->login();
				return $this->redirect(Director::absoluteBaseURL());
			}
		}

		die('Invalid token');
	}
}
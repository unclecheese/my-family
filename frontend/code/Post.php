<?php

class Post extends DataObject
{

	private static $db = array (
		'Content' => 'Markdown',
		'Author' => 'Varchar',
	);


	private static $has_one = array (
		'Author' => 'Member',
		'Thread' => 'Thread'
	);

	private static $has_many = array (
		'Flags' => 'MarkedPost'
	);


	private static $many_many = array (
		'Attachments' => 'File',		
	);


	private static $belongs_many_many = array (
		'UnreadMembers' => 'Member'
	);


	private static $extensions = array (
		"FulltextSearchable('Content')"
	);


	private static $default_sort = 'Created ASC';

	public function getCMSFields() {
		return FieldList::create(TabSet::create("Root"))
			->htmlEditor('Content')
			->dropdown('AuthorID','Author', Member::get()->map('ID','Name'))
			->upload('Attachments');
	}


	public function IsFirstPost() {
		return $this->ID == $this->Thread()->FirstPostID;
	}


	public function onAfterWrite() {
		parent::onAfterWrite();

		$t = $this->Thread();
		$t->LastEdited = SS_Datetime::now()->Rfc2822();
		$this->UnreadMembers()->setByIDList(
			Member::get()
				->exclude(array('ID' => $this->AuthorID))
				->column('ID')
		);
		$t->write();

	}
}
<?php

class DailyEmailTask extends BuildTask {

	
	public function run($request) {
		$daySent = 0;
		foreach(Member::get()->filter('EmailNotifications', true) as $m) {
			$buffer = $m->EmailPeriod*86400;
			$diff = (time() - strtotime($m->LastVisited));
			if($diff < $buffer) continue;

			$filter = array ('Created:GreaterThan' => $m->LastVisited);

			$unreadPosts = $m->UnreadPosts()->filter($filter);
			$flags = MarkedPost::get()->filter($filter);
			$faves = FavoritedThread::get()->filter($filter);

			echo $unreadPosts->count() . " unread posts".PHP_EOL;
			echo $flags->count() . " new flags".PHP_EOL;
			echo $faves->count() . " new favourites".PHP_EOL;


			if(!$unreadPosts->exists() && !$flags->exists() && !$faves->exists()) {
				echo "Nothing new.";
				die();
			}

			$e = Email::create(
				'MyFamily <notifications@ballsandnuts.com>',
				$m->Email,
				'New stuff'
			)
			->setUserTemplate('digest')
			->populateTemplate(array(
				'Member' => $m,
				'Posts' => $unreadPosts,
				'Flags' => $flags->exclude('MemberID', $m->ID),
				'Faves' => $faves->exclude('MemberID', $m->ID)
			))
			->send();

			echo "sent to {$m->getName()}".PHP_EOL;
			$daySent++;

		}

		die("Sent $daySent emails");

	}
}

<?php


class MemberExtension extends DataExtension {

	private static $db = array (
		'EmailNotifications' => 'Boolean',
		'EmailPeriod' => 'Int',
		'Token' => 'Varchar',
	);


	private static $many_many = array (
		'UnreadThreads' => 'Thread',
		'UnreadPosts' => 'Post'
	);


	public function assignToken() {
		$this->owner->Token = md5(uniqid().time());
		$this->owner->write();
	}
}
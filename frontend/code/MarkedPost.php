<?php

class MarkedPost extends DataObject {

	private static $db = array (
		'Status' => 'Varchar',
	);


	private static $has_one = array (
		'Post' => 'Post',
		'Member' => 'Member'
	);

	public function NiceStatus() {
		switch($this->Status) {
			case "dusoism":
				return "DUSOISM";
			case "robson":
				return "ROBSON DEFENSE";
			case "carlino":
				return "GROSS EXAGGERATION";
			case "shadows":
				return "FROM THE SHADOWS";
			case "steve":
				return "OH, STEVE...";
		}
	}
}